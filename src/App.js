import 'bootstrap/dist/css/bootstrap.min.css'
import Display from "./components/Display";
import Parent from "./components/Parent";


function App() {
  return (
    <div>
      <Parent/>
    </div>
  );
}

export default App;
