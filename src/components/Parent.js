import { Component } from "react";
import Display from "./Display";


class Parent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            childDisplay: false,
            buttonColor: "blue",
            buttonText: "Create Component"
        }
        
    }


    clickChangedHandler = () => {
        console.log('Nút được ấn!')
        if (this.state.childDisplay) {
            this.setState({
                childDisplay: false,
                buttonColor: "blue",
                buttonText: "Create Component"
            })
        } else {
            this.setState({
                childDisplay: true,
                buttonColor: "red",
                buttonText: "Destroy Component"
            })
        } 
        
    }



    render() {
        return(
            <>
                
                <button onClick={this.clickChangedHandler} style={{ backgroundColor: this.state.buttonColor}}>{this.state.buttonText}</button>
                {this.state.childDisplay ? <Display/> : null}
                

            </>
        )
    }
}

export default Parent;